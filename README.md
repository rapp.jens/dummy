# dummy - a simple git server helping tool
==========================================

This tool is meant to help administrating a simple ssh based git server.
You can add ssh keys to the user account your repositories run on
as well as creating or removing repositories.

    Attention:
    Due to the way of managing repositories on a linux user account, there is
    no rights management. Every user basically has full access to any
    repository.

## dummy installation
To setup a git ssh server using dummy, follow these instructions.

Since there are many steps which must be done as root, I recommend to become
root by using

    $ sudo bash

First, we'll make shure that everything neccessary exists.

    # apt install git python3  # or yum install, pacman -S, apk get, ...

Then we add a user named git (or whatever).

    # adduser git

next we'll clone dummy as user git as git-shell-commands dir which makes it
usable through git-shell

    # su - git
    $ git clone https://gitlab.com/rapp.jens/dummy.git git-shell-commands
    $ PATH=$PATH:git-shell-commands dummy user get

The last one creates the file ~/.dummy\_config which we can edit now.

    [repositories]
    gitpath = /usr/bin/git
    repopath = /home/git/repo
    repourl = git@[your_gitserver]:repo

    [users]
    authorized_keys = /home/git/authorized_keys

- gitpath is the path to your git command itself
- repopath is where your repositories will be created. Take care that
this is an existing path.
- repourl is the ssh address of your server. Dummy helps you using your
repositories by telling you how to find them
- authorized\_keys tells you where your keys file is

Now we're almost done. Just take care that your repopath exists.

    $ mkdir ~/repo
    $ exit

You should now create a ssh keypair if you don't already have one.

    my_pc# ssh-keygen -t rsa
    Generating public/private rsa key pair.
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in testkey
    Your public key has been saved in ~/.ssh/id_rsa.pub
    The key fingerprint is:
    SHA256:6hmcBgdh2lCDLEldZ8eMJDlvwhjluVFuC4j2vcuGsgw tecdroid@my_pc
    The key's randomart image is:
    +---[RSA 3072]----+
    |.+ooB+o=+.       |
    |o +Bo=B..o       |
    | +..*=oo         |
    |. ...+=o.        |
    |   ..o+.S        |
    |     +.o         |
    |E   ..*          |
    |o. ..+.o         |
    | oo .oo          |
    +----[SHA256]-----+

And copy it to your git login

    my_pc$ ssh-copy-id git@your_gitserver
    usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/tecdroid/.ssh/id_rsa.pub"
    /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
    /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
    git@localhost's password:

    Number of key(s) added: 1

    Now try logging into the machine, with:   "ssh 'git@your_gitserver'"
    and check to make sure that only the key(s) you wanted were added.


Last thing to do here is telling the user git to use git-shell

    # vi /etc/passwd
    ...
    git:x:1001:1001:,,,:/home/git:/usr/bin/git-shell


## dummy usage
now you can login to your git shell to administer your repositories

    my_pc$ ssh git@your_gitserver
    git@your_gitserver's password:
    git> dummy repo get
    Sorry, no repositories there.


### add, list and delete users
Adding a user requires to have his ssh public key.
Next we need to add this to our servers user list. The easiest way to do this is ssh:

    my_pc$ ssh git@your_gitserver "dummy user add Heinz '$(cat id_rsa_heinz.pub)'"
    User Heinz added

all of the following commands could also be set via ssh in that manner:

    my_pc$ ssh git@your_gitserver "dummy order"

To make it easier for me, I pretend to be logged in as user git

To list all existing users, just type

    git> dummy user get
    Heinz
    Paul
    Dennis

Deleting users is also simple:

    git> dummy user del Paul
    User Paul successfully deleted.
    git> dummy user get
    Heinz
    Dennis


### administering repositories
To create a repository just use the function repo add

    git> dummy repo add myrepo
    Repository myrepo.git successfully created.
    You can access the repository using the url git@your_gitserver:repo/myrepo.git

Deleting repositories is also possible:

    git> dummy repo del myrepo
    Repository myrepo.git deleted.

Listing repositories is also possible

    git> dummy repo get
    dummy
    myrepo
    testrepo


## have fun
If there is anything missing, tell me
