import argparse
import logging

from os.path import exists
from .config import get_config

keyfile = get_config()['users']['authorized_keys']

def list_users():
    ''' list all users
    '''
    values = {}
    if not exists(keyfile):
        logging.warning(f'Authorized keys file does not yet exist')
        return values

    with open(keyfile) as fp:
        for line in fp.readlines():
            if len(line) != 0:
                key, name = line.strip().rsplit(' ', maxsplit=1)
                logging.debug(f'key for {name} "{key}"')
                values[name] = key
    return values

def save_users(users):
    ''' save user data
    '''
    logging.debug(f'saving data')
    with open(keyfile, 'w') as fp:
        for name, key in users.items():
            fp.write(f'{key} {name}\n')


def add_user(params):
    ''' add a user to the authorized keys
    '''
    username = params.user
    key, name = params.key.rsplit(' ', maxsplit=1)
    users = list_users()

    # check for existing user
    if username in users.keys():
        if 'force' not in params or params.force is False:
            logging.error(f'username {username} already exists.')
            return
        logging.debug(f'username {username} already exists. Exchanging key')

    logging.debug (f'setting {username} with new key:\n{key}')
    users[username] = key

    save_users(users)
    print(f'User {username} added')


def del_user(params):
    ''' delete a user
    TODO: delete a user
    '''
    users = list_users()
    username = params.user
    if username not in users:
        logging.warning(f'user {username} does not exist')
        return
    del users[username]
    save_users(users)
    print (f'User {username} successfully deleted.')


def get_users(params):
    ''' list all users from authorized keys
    '''
    for user in list_users().keys():
        print(user)


def add_subparser(parser):
    ''' create sub parser for user functions
    '''
    user = parser.add_parser('user')
    sp = user.add_subparsers(dest='action')
    uadd = sp.add_parser('add', help='add a user')
    uadd.add_argument('user', help='username')
    uadd.add_argument('key', help='the ssh key')
    uadd.add_argument(
            '-f', '--force',
            help='overwrite existing users',
            action='store_true'
            )
    udel = sp.add_parser('del', help='delete a user')
    udel.add_argument('user', help='username')
    uget = sp.add_parser('get', help='list current users')


def run(arguments):
    ''' run the user functions
    '''
    if arguments.action == 'add':
        add_user(arguments)
    elif arguments.action == 'del':
        del_user(arguments)
    elif arguments.action == 'get':
        get_users(arguments)
    pass

