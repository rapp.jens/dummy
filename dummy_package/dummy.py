import sys
import logging
import argparse

from . import user
from . import repo

def main():
    ''' run the base program
    '''
    # set up argument parsing
    parser = argparse.ArgumentParser(
        description='help using gitsh on minimalistic git servers'
        )
    parser.add_argument('-v', '--verbose', action='store_true', help='Tell me more')
    subparsers = parser.add_subparsers(dest='function')
    user.add_subparser(subparsers)
    repo.add_subparser(subparsers)
    args = parser.parse_args()

    # set up logging
    # could be a ternary operation
    loglevel = logging.INFO
    if 'verbose' in args and args.verbose is True:
        loglevel = logging.DEBUG

    log = logging.basicConfig(level=loglevel)

    # run function
    if args.function == 'user':
        user.run(args)
    elif args.function == 'repo':
        repo.run(args)
    return 0

