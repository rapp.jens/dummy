import os
from os.path import expanduser, exists
import logging
import configparser

configfile = expanduser('~/.dummy_conf')
__config = None

user = os.getlogin()
repobase = 'repo'

__baseconfig = {
        'repositories' : {
           'gitpath' : '/usr/bin/git',
           'repopath' : expanduser(f'~/{repobase}'),
           'repourl' : f'{user}@localhost:{repobase}'
        },
        'users' : {
            'authorized_keys' : expanduser('~/.ssh/authorized_keys'),
        }
}

def get_config():
    ''' get the global config
    if none exists, create a new one
    '''
    global __config
    if __config is None:
        __config = configparser.ConfigParser()
        __config.read_dict(__baseconfig)
        if exists(configfile):
            __config.read(configfile)
        else:
            logging.info(f'config file not found. creating one..')
            with open(configfile, 'w') as fp:
                __config.write(fp)

    return __config


