import os
import argparse
import subprocess
import logging
from dummy_package import config

cfg = config.get_config()

def get_gitpath():
    ''' get the git binary base path
    '''
    gitpath = cfg['repositories']['gitpath']
    logging.debug(f'git basepath: {gitpath}')
    return gitpath


def get_repopath(reponame=None):
    ''' get the system path to the repository `reponame`
    '''
    rpath = cfg['repositories']['repopath']
    if reponame is not None:
        rpath = os.path.join(rpath, reponame)
    logging.debug(f'repository path: {rpath}')
    return rpath


def get_repourl(reponame):
    ''' get the ssh url to the repository `reponame`
    '''
    base = cfg['repositories']['repourl']
    rpath = os.path.join(base, reponame)
    logging.debug(f'repository path: {rpath}')
    return rpath


def run_process(order):
    ''' run a system process to handle git commands
    '''
    result = subprocess.run(
            order,
            shell=True,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE
    )
    if result.returncode == 0:
        logging.debug(result.stdout.decode())
    else:
        logging.error(result.stdout.decode())
    return result.returncode


def add_repo(params):
    ''' add a repository by name
    '''
    reponame = f'{params.repo}.git'
    repopath = get_repopath(reponame)

    if os.path.exists(repopath):
        if 'force' not in params or params.force == False:
            logging.error('Repository already exists. Use -f to re-init')
            return

    order = f'{get_gitpath()} init --bare {repopath}'
    if run_process(order) == 0:
        print(f'Repository {reponame} successfully created.')
        repourl = get_repourl(reponame)
        print(f'You can access the repository using the url {repourl}')
    else:
        logging.error(f'Could not create repository.')


def del_repo(params):
    ''' delete a repository by name
    '''
    reponame = f'{params.repo}.git'
    order = f'rm -rf {get_repopath(reponame)}'
    if run_process(order) == 0:
        print(f'Repository {reponame} deleted.')
    else:
        logging.error(f'Could not delete repository.')



def get_repos(params):
    ''' list all repositories
    '''
    repos = os.listdir(get_repopath())
    if len(repos) == 0:
        print('Sorry, no repositories there.')
    else:
        for file in repos:
            if file.endswith('.git'):
                print(file.replace('.git', ''))


def add_subparser(parser):
    ''' add the functions sub parser
    '''
    repo = parser.add_parser('repo')
    sp = repo.add_subparsers(dest='action')
    radd = sp.add_parser('add', help='add a repository')
    radd.add_argument('repo', help='repository name')
    radd.add_argument(
            '-f', '--force',
            help='re-init if repository already exists',
            action='store_true'
            )
    rdel = sp.add_parser('del', help='delete a repository')
    rdel.add_argument('repo', help='repository name')
    rget = sp.add_parser('get', help='list current repositories')


def run(arguments):
    ''' run the sub functions
    '''
    if arguments.function == 'repo':
        if arguments.action == 'add':
            add_repo(arguments)
        elif arguments.action == 'del':
            del_repo(arguments)
        elif arguments.action == 'get':
            get_repos(arguments)


